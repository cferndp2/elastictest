from openpyxl import load_workbook
import requests

URL_INDEX = "http://localhost:9200/lar_report/_doc"


def getCC(name):

    if name == "111  -  C.C. GRAN VIA DE VIGO":
        return {
            "lat": 42.220631,
            "lon": -8.723530
        }
    elif name == "93  -  C.C. ALBACENTER":
        return {
            "lat": 39.820660,
            "lon": -6.774610
        }
    elif name == "99  -  C.C. ALBACENTER SHOPPING CENTRES":
        return {
            "lat": 39.820660,
            "lon": -6.774610
        }
    elif name == "95  -  C.C. ANECBLAU":
        return {
            "lat": 41.282636,
            "lon": 1.985052
        }
    elif name == "100  -  C.C. AS TERMAS":
        return {
            "lat": 43.038072,
            "lon": -7.566644
        }
    elif name == "103  -  GASOLINERA AS TERMAS":
        return {
            "lat": 43.038072,
            "lon": -7.566644
        }
    elif name == "107  -  C.C. EL ROSAL":
        return {
            "lat": 42.555730,
            "lon": -6.602827
        }
    elif name == "55  -  C.C. PORTAL DE LA MARINA":
        return {
            "lat": 38.816944,
            "lon": 0.021758
        }
    elif name == "102  -  HIPER PORTAL DE LA MARINA":
        return {
            "lat": 38.816944,
            "lon": 0.021758
        }
    elif name == "156  -  P.C. RIVAS FUTURA":
        return {
            "lat": 40.336721,
            "lon": -3.531727
        }
    elif name == "158  -  P.C. VIDANOVA PARC":
        return {
            "lat": 39.672319,
            "lon": -0.265501
        }
    elif name == "163  -  GASOLINERA VIDANOVA":
        return {
            "lat": 39.672319,
            "lon": -0.265501
        }
    elif name == "88  -  P.C. VISTAHERMOSA":
        return {
            "lat": 38.370000,
            "lon": -0.466624
        }
    elif name == "130  -  C.C. LAGOH":
        return {
            "lat": 37.341540,
            "lon": -5.988513
        }
    elif name == "157  -  C.C. LA ABADIA":
        return {
            "lat": 39.904772,
            "lon": -4.017386
        }
    elif name == "124  -  P.C. LA ABADIA":
        return {
            "lat": 39.904772,
            "lon": -4.017386
        }
    elif name == "MEGAPARK - FOC":
        return {
            "lat": 40.545110,
            "lon": -3.612423
        }
    elif name == "MEGAPARK - OCIO":
        return {
            "lat": 40.545110,
            "lon": -3.612423
        }
    elif name == "MEGAPARK - RW":
        return {
            "lat": 40.545110,
            "lon": -3.612423
        }
    else:
        return None


def saveDocument(document):
    response = requests.post(URL_INDEX, json=document)

    return response.status_code


wb2 = load_workbook('data.xlsx')
ws = wb2.active

rowCount = 0

for row in ws.iter_rows(min_row=2):
    document = {
        "name": row[0].value,
        "unit": row[1].value,
        "brand": row[2].value,
        "activity": row[3].value,
        "GLA": row[5].value,
        "MGRYear": row[6].value,
        "MGRMonth": row[7].value,
        "MGRMonthPsm": row[8].value,
        "TORAccount": row[9].value,
        "TOREstimated": row[10].value,
        "ERVPA": row[11].value,
        "CC": getCC(row[0].value)
    }

    result = saveDocument(document)

    if result == 201:
        print(f'Row {rowCount} saved')
    else:
        print(f'Error in row {rowCount}')

    rowCount += 1
